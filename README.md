# sinatra-child

> Better Family Demolition LLC's extension of the Sinatra Theme for Wordpress.

## Install

> ![alert][octicon-alert] You need filesystem access to install the **sinatra-child** theme.

1.  Open your WordPress site directory (e.g., via <samp>SSH</samp>).

1.  Create a new folder called <samp>sinatra-child</samp> in your 
    <samp>themes</samp> directory, located at <samp>wp-content/themes</samp>.

1.  Add the files <samp>functions.php</samp> and <samp>style.css</samp> to
    the <samp>wp-content/themes/sinatra-child</samp> directory.

## Activate the **sinatra-child** theme

Your child theme is now ready for activation. 

1.  Log in to your site’s Administration Screen.

1.  Go to **Administration Screen > Appearance > Themes**. You should see your
    child theme listed and ready for activation.
    
    _Note_:
    
    > If your WordPress installation is multi-site enabled, then you may need
    > to switch to your network Administration Screen to enable the theme (within
    > the Network Admin Themes Screen tab). You can then switch back to your
    > site-specific WordPress Administration Screen to activate your child theme.

---

![info][octicon-info] You may need to re-save your menu from **Appearance > Menus**
and theme options (including background and header images) after activating the child
theme.

---

## License

[MIT](LICENSE) © Better Family Demolition LLC.

<!-- 🚫 Do not remove this line or anything beneath it. 🚫 -->

[octicon-alert]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/alert.svg
[octicon-info]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/info.svg
[octicon-terminal]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/terminal.svg
[octicon-upload]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.5.0/svg/cloud-upload.svg
